﻿using Elastic.Channels;
using Elastic.Ingest.Elasticsearch;
using Elastic.Ingest.Elasticsearch.DataStreams;
using Elastic.Serilog.Sinks;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Information()
    .Enrich.FromLogContext()
    .WriteTo.Elasticsearch(new[] { new Uri("http://localhost:9200") }, opts =>
    {
        opts.DataStream = new DataStreamName("logs", "dataSet", "namespace");
        opts.BootstrapMethod = BootstrapMethod.Failure;
        opts.ConfigureChannel = channelOpts =>
        {
            channelOpts.BufferOptions = new BufferOptions()
            {
                ExportMaxConcurrency = 10
            };
        };
    })
    .CreateLogger();

while(true)
{
    Log.Error("error mark");

    Log.Warning("warning mark");

    Log.CloseAndFlush();
}


string? input = Console.ReadLine();

Log.CloseAndFlush();
